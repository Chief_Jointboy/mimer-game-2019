"use strict";
const assetsPath = "../../../../mimer-assets-pre/";

let score= 0;
let scoreMsg="Score: ";
let scoreText=null;


export class InvaderScene extends Phaser.Scene {

        constructor (config)
        {
            super(config);
            Phaser.Scene.call(this, { key: "MazeToGoal", active: true });
            this.gameOn=true;
            this.my_buttons = [];
            this.step=4;
            this.player=null; 
            this.spacebar;
            this.invaders = null;
         
        }
        
        preload() {
			this.load.spritesheet('mushrooms', assetsPath + 'icons/mushrooms-diffrent-color-128x128-7x3.png', { frameWidth: 128, frameHeight: 128});
			this.load.image('player', assetsPath + 'ships/nightraider-224x154.png');
		}

        create ()
        {
            //For the player
            
			this.player = this.physics.add.image(400, 550, 'player');
			this.player.setScale(0.25).setRotation(-Math.PI/2);
						
			this.cursors = this.input.keyboard.createCursorKeys();
			this.spacebar = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.SPACE);
			
			// Create a bullet
			this.pen = this.make.graphics({x: 0, y: 0, add: false});
            this.pen.fillStyle(0xFF0000, 1.0);
            this.pen.fillCircle(10, 10, 10, 10);
            this.pen.generateTexture('bullet', 20, 20);
			
			//For the invader
            
          	this.invaders = this.physics.add.group({ key: 'mushrooms', frame: 0, repeat: 9, setXY: { x: 100, y: 150, stepX: 50}, setScale: { x: 0.30} });
            this.invaders .setVelocityX(20); 
      
            scoreText = this.add.text(16, 16, 'score: 0', { fontSize: '32px', fill: '#ffffff' });
         }

        update()
        { 
          
		  
          if(this.gameOn) {
	         if (this.cursors.left.isDown)
             {
                 this.player.x-=this.step;
             }
             else if (this.cursors.right.isDown)
             {
                 this.player.x+=this.step;
             }
             else if (Phaser.Input.Keyboard.JustDown(this.spacebar))
             {
                 this.shoot();
               	 
             }
             if (this.invaders.x > 100)
             {
                 this.invaders .setVelocityX(-50);
             }
             if (score === 100)
             {
              game.destroy()
              this.add.text(250, 250, 'YOU WON!!!', { fontSize: '60px', fill: '#ffffff' }); 
             }
            				
		}
		}
		
		shoot()
        {
            let bullet = this.physics.add.image(this.player.x, this.player.y, 'bullet');
            bullet.body.velocity.y = -400;

            this.physics.add.overlap(bullet, this.invaders, this.hit, this.score); 
	    }
	    
	    hit(bullet, invader)
        {
            console.log("hit");
            bullet.destroy();
            invader.destroy();
        	score += 10;                           
            scoreText.setText(scoreMsg + score);   
	       
        }
  		
  		
}
